#include <stdio.h>
#include <stdlib.h>
#include "bitop.h"

void tests();

void main() {
  tests();
}

void tests(){
  unsigned long a = 0;
  SetBit(&a, 0);
  if (a == 1) { printf("SetBit() ok\n----\n"); }
  else { printf("SetBit() falhou\n----\n"); }

  a = 4;
  ResetBit(&a, 2);
  if (a == 0) { printf("ResetBit() ok\n----\n"); }
  else { printf("ResetBit() falhou\n----\n"); }

  a = 5;
  ToggletBit(&a, 0);
  if (a == 4) { printf("ToggletBit() ok\n----\n"); }
  else { printf("ToggletBit() falhou\n----\n"); }

  unsigned char c = ReadBit(&a, 2);
  if (c == 1) { printf("ReadBit() ok\n----\n"); }
  else { printf("ReadBit() falhou\n----\n"); }

  a = 53;
  unsigned long l = ReadBitSlice(&a, 1, 4);
  if (l == 10) { printf("ReadBitSlice() ok\n----\n"); }
  else { printf("ReadBitSlice() falhou\n----\n"); }

  if (IsLittleEndian()) printf("Is Little Endian\n");
  else printf("Is not Litte Endian\n");
}
