#include "bitop.h"
#include <stdio.h>

/**
@brief Seta o bit da posição de memória apontada por p
@param p Ponteiro para variável de 32bits cujo bit será setado
@param bitn Posição do bit (0 a 31) a ser setado
*/
void SetBit(unsigned long *p, unsigned char bitn) {
  *p |= (1<<bitn);
}

/**
@brief Reseta o bit da posição de memória apontada por p
@param p Ponteiro para variável de 32bits cujo bit será resetado
@param bitn Posição do bit (0 a 31) a ser resetado
*/
void ResetBit(unsigned long *p, unsigned char bitn) {
  *p &= ~(1<<bitn);
}

/**
@brief Inverte um bit da posição de memória apontada por p
@param p Ponteiro para variável de 32bits cujo bit será invertido
@param bitn Posição do bit (0 a 31) a ser invertido
*/
void ToggletBit(unsigned long *p, unsigned char bitn) {
  *p ^= (1<<bitn);
}

/**
@brief Ler o bit da posição de memória apontada por p
@param p Ponteiro para variável de 32bits cujo bit será lido
@param bitn Posição do bit (0 a 31) a ser lido
@return retorna o estado (0 ou 1) do bit da posição de memória apontada por p
*/
unsigned char ReadBit(unsigned long *p, unsigned char bitn) {
  return (*p>>bitn)&1;
}

/**
@brief Ler uma fatia de bits da posição bitstart até bitend
@param p Ponteiro para variável de 32bits de onde a fatia de bits será lida
@param bitstart Posição do bit (0 a 31) do primeiro bit a ser lido
@param bitend Posição do bit (0 a 31) do último bit a ser lido
@return retorna os bits de bitstart até bitend
*/
unsigned long ReadBitSlice(unsigned long *p, unsigned char bitstart, unsigned char bitend) {
  return (*p >> bitstart) & (1 << (bitend - bitstart + 1)) - 1;
}

/**
@brief Detecta se a arquitetura corrente armazena os dados  em formato little endian ou big endian
@return Retorna 1 para Little endian e 0  para Big Endian
*/
unsigned char IsLittleEndian (void) {
  unsigned int x = 1;
  unsigned char *c = (unsigned char*) &x;
  return *c;
}

void PrintDecToBin(unsigned long n) {
    int binaryNum[32];
    int i = 0;
    while (n > 0) {
      binaryNum[i] = n % 2;
      n = n / 2;
      i++;
    }

    for (int j = i - 1; j >= 0; j--) {
      printf("[%d]", binaryNum[j]);
    }
    printf("\n");
}
