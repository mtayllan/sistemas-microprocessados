#ifndef BITOP_H // guardas de cabeçalho, impedem inclusões cíclicas
#define BITOP_H

void SetBit(unsigned long *p, unsigned char bitn);
void ResetBit(unsigned long *p, unsigned char bitn);
void ToggletBit(unsigned long *p, unsigned char bitn);
unsigned char ReadBit(unsigned long *p, unsigned char bitn);
unsigned long ReadBitSlice(unsigned long *p, unsigned char bitstart, unsigned char bitend);
unsigned char IsLittleEndian(void);
void PrintDecToBin();

#endif
