################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/BlinkLed.c \
../src/Timer.c \
../src/_write.c \
../src/main.c 

S_UPPER_SRCS += \
../src/IsLittleEndian.S \
../src/ReadBit.S \
../src/ReadBitSlice.S \
../src/ResetBit.S \
../src/SetBit.S \
../src/ToggleBit.S \
../src/fatorial.S \
../src/fibonacci.S \
../src/mymemcmp.S \
../src/mymemcpy.S \
../src/mymemset.S \
../src/mystrcmp.S \
../src/mystrcpy.S \
../src/mystrlen.S \
../src/soma.S 

OBJS += \
./src/BlinkLed.o \
./src/IsLittleEndian.o \
./src/ReadBit.o \
./src/ReadBitSlice.o \
./src/ResetBit.o \
./src/SetBit.o \
./src/Timer.o \
./src/ToggleBit.o \
./src/_write.o \
./src/fatorial.o \
./src/fibonacci.o \
./src/main.o \
./src/mymemcmp.o \
./src/mymemcpy.o \
./src/mymemset.o \
./src/mystrcmp.o \
./src/mystrcpy.o \
./src/mystrlen.o \
./src/soma.o 

S_UPPER_DEPS += \
./src/IsLittleEndian.d \
./src/ReadBit.d \
./src/ReadBitSlice.d \
./src/ResetBit.d \
./src/SetBit.d \
./src/ToggleBit.d \
./src/fatorial.d \
./src/fibonacci.d \
./src/mymemcmp.d \
./src/mymemcpy.d \
./src/mymemset.d \
./src/mystrcmp.d \
./src/mystrcpy.d \
./src/mystrlen.d \
./src/soma.d 

C_DEPS += \
./src/BlinkLed.d \
./src/Timer.d \
./src/_write.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F030 -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f0-stdperiph" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m0 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -x assembler-with-cpp -DDEBUG -DUSE_FULL_ASSERT -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DSTM32F030 -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f0-stdperiph" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


