.syntax unified
.text
.macro ret
	bx lr
.endm

.globl mystrlen
mystrlen:
	movs r1, #0 //contador
	laco:
		ldrb r2, [r0]
		cmp r2, #0
		beq retorno
		adds r0, r0, #1
		adds r1, r1, #1
		b laco
	retorno:
		movs r0, r1
		ret

// r0 o ponteiro pra str
// r1 tem o caractere pra colocar
// r2 tem a quantidade de repeticoes
