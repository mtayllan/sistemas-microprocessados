.syntax unified
.text
.macro ret
	bx lr
.endm

.globl ReadBitSlice
ReadBitSlice:
	ldr r3, [r0] // r2 = *r0
	lsrs r3, r3, r1 // *p >> bitstart
	subs r2, r2, r1 // res = bitend - bitstart
	adds r2, r2, #1 // res + 1
	movs r1, #1 // r1 = 1
	lsls r1, r1, r2 // r1 = 1 << r2
	subs r1, r1, #1 // r1 - 1
	ands r3, r3, r1 // r3 = r3 & r1
	movs r0, r3 // r0 = r3
	ret

// r0 address
// r1 bit start
// r2 bit end
// (*p >> bitstart) & (1 << (bitend - bitstart + 1)) - 1;
