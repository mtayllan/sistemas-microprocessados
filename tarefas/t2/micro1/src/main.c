#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

void testNormalOperations();
void testBitOperations();
void testMemOperations();

int main(int argc, char* argv[])
{
  testNormalOperations();
  testBitOperations();
  testMemOperations();
  trace_printf("\nTestes finalizado....\n");
}

unsigned long fibonacci(unsigned short n);
unsigned long fatorial(unsigned char n);
void soma(int *p1, int *p2,int n);

void testNormalOperations(){
	trace_printf("\nTestando Operações Normais....\n");
	trace_printf("Fibonacci 14 = %lu\n", fibonacci(14));
	trace_printf("Fatorial 6 = %lu\n", fatorial(6));
	int v0[3] = {1,2,3};
	int v1[3] = {4,5,6};
	soma(v0, v1, 3);
	int i;
	for (i=0; i<3; i++){
		trace_printf("Somados %d = %d\n", i+1, v0[i]);
	}

	trace_printf("\n-----\n");
}

void SetBit(unsigned long *p, unsigned char bitn);
void ResetBit(unsigned long *p, unsigned char bitn);
void ToggleBit(unsigned long *p, unsigned char bitn);
unsigned char ReadBit(unsigned long *p, unsigned char bitn);
unsigned long ReadBitSlice(unsigned long *p, unsigned char bitstart, unsigned char bitend);
unsigned char IsLittleEndian (void);

void testBitOperations(){
	trace_printf("\nTestando Operações de Bit....\n");
	unsigned long a = 0;
	SetBit(&a, 1);
	if (a == 2) { trace_printf("SetBit() ok\n"); }
	else { trace_printf("SetBIT FALHOU\n"); }

	a = 4;
	ResetBit(&a, 2);
	if (a == 0) { trace_printf("ResetBit() ok\n"); }
	else { trace_printf("ResetBit() %lu  falhou\n----\n", a); }

	a = 5;
	ToggleBit(&a, 0);
	if (a == 4) { trace_printf("ToggletBit() ok\n"); }
	else { trace_printf("ToggletBit() falhou\n---\n"); }

	unsigned char c = ReadBit(&a, 0);
	if (c == 0) { trace_printf("ReadBit() ok\n"); }
	else { trace_printf("ReadBit() falhou\n----\n"); }

	a = 53;
	unsigned long l = ReadBitSlice(&a, 1, 4);
	if (l == 10) { trace_printf("ReadBitSlice() ok\n"); }
	else { trace_printf("ReadBitSlice() falhou l=%lu\n---\n", l); }

	if (IsLittleEndian()) trace_printf("Is Little Endian\n");
	else trace_printf("Is not Litte Endian\n");

	trace_printf("\n-----\n");
}

void *mymemset(void *ptr, int value, size_t num);
void *mymemcpy (void *destination, const void *source, size_t num );
int mymemcmp(const void *ptr1, const void *ptr2, size_t num);
char *mystrcpy(char *dest, const char *src);
int mystrcmp(const char *str1, const char *str2);
size_t mystrlen(const char *str);

void testMemOperations() {
	trace_printf("\nTestando Operações de Memória....\n");
	char str[] = "antes de setar";
	mymemset (str,'u',5);
	trace_printf("new = %s\n", str);

	char src[] = "Vou copiar não";
	char dest[100];
	mymemcpy(dest, src, 10);
	trace_printf("Isso foi copiado: %s\n", dest);

	char str1[] = "125";
	char str2[] = "124";
	int cmp = mymemcmp(str1, str2, 2);
	trace_printf("Resultado = %d\n", cmp);

	char str3[]= "sou uma string";
	char str4[20];
	mystrcpy(str4, str3);
	trace_printf("mystrcpy = %s\n", str4);

	char str5[] = "string5";
	char str6[] = "string6";
	int comp =  mystrcmp(str5, str6);
	trace_printf("Resultado = %d\n", comp);

	char str7[] = "len123";
	int tam = mystrlen(str7);
	trace_printf("Tamanho de %s é %d\n", str7, tam);


	trace_printf("Operações de memoria finalizado\n");
}

#pragma GCC diagnostic pop
