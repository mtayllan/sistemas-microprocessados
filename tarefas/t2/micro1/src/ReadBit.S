.syntax unified
.text
.macro ret
	bx lr
.endm

.globl ReadBit
ReadBit:
	ldr r2, [r0]
	lsrs r2, r2, r1
	movs r3, #1
	ands r2, r2, r3
	movs r0, r2
	ret
