.syntax unified
.text
.macro ret
	bx lr
.endm

.globl soma
soma:
	push {r4-r7}
	subs r2, r2, #0
	beq retorno
	laco:
		ldr r3, [r0]
		ldr r4, [r1]
		adds r5, r3, r4
		str r5, [r0]
		adds r0, r0, #4
		adds r1, r1, #4
		subs r2, r2, #1
		bne laco
	retorno:
		pop {r4-r7}
		ret
