.syntax unified
.text
.macro ret
	bx lr
.endm

.globl fibonacci
fibonacci:
	push {r4-r7}
	subs r2, r0, #1 // soma r0 com 1 e coloca em r2
	beq end1 // retorna se instrução anterior retorna 0
	subs r1, r1, r1 // zera
	subs r2, r2, r2 // zera
	adds r1, r1, #1 // primeiro elemento
	adds r2, r2, #1 // segundo elemento
	adds r3, r1, r2 // inicia contador em 2
	subs r3, r0, r3 // subtrai n-2
	beq end1
	laco: subs r4, r4, r4 // zera variavel temporaria para armazenar elemento antigo
		adds r4, r4, r2 // armazena segundo elemento
		adds r2, r2, r1 // soma os dois elementos atuais e bota no segundo
		subs r1, r1, r1 // zera o primeiro elemento
		adds r1, r1, r4 // joga o valor temporario no r1
		subs r3, r3, #1
		bne laco
	b end2
	end1:
		subs r0, r0, r0
		adds r0, r0, #1
		pop {r4-r7}
		ret
	end2: subs r1, r1, r1
		adds r0, r1, r2
		pop {r4-r7}
		ret
